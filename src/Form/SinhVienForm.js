import React, { Component } from "react";

export default class SinhVienForm extends Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
  }
  state = {
    user: {
      svId: "",
      phone: "",
      name: "",
      email: "",
    },
  };
  handleGetSvForm = (e) => {
    console.log(e.target.value);
    // name:key , đổi tên name thành key
    let { value, name: key } = e.target;
    let cloneUser = { ...this.state.user };
    cloneUser[key] = value;
    this.setState({ user: cloneUser }, () => {
      console.log(this.state.user);
    });
  };
  handleSvSubmit = () => {
    let newUser = { ...this.state.user };
    this.props.handleSinhVienAdd(newUser);
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.svEdited != null) {
      this.setState({ user: nextProps.svEdited });
    }
  }
  render() {
    return (
      <div className="text-left mb-2">
        <div className="banner bg-dark text-light py-2 px-2">
          Thông tin sinh viên
        </div>
        <div className="form-group row">
          <div className="col-6">
            <label htmlFor="">Mã SV</label>
            <input
              value={this.state.user.svId}
              onChange={this.handleGetSvForm}
              ref={this.inputRef}
              type="text"
              className="form-control"
              name="svId"
              placeholder="Mã sinh viên"
            />
            <label htmlFor="">Số điện thoại</label>
            <input
              value={this.state.user.phone}
              onChange={this.handleGetSvForm}
              type="text"
              className="form-control"
              name="phone"
              placeholder="Số điện thoại"
            />
          </div>
          <div className="col-6">
            <label htmlFor="">Họ tên</label>
            <input
              value={this.state.user.name}
              onChange={this.handleGetSvForm}
              type="text"
              className="form-control"
              name="name"
              placeholder="Họ tên sinh viên"
            />
            <label htmlFor="">Email</label>
            <input
              value={this.state.user.email}
              onChange={this.handleGetSvForm}
              type="text"
              className="form-control"
              name="email"
              placeholder="Email"
            />
          </div>
        </div>
        <button className="btn btn-warning mr-2" onClick={this.handleSvSubmit}>
          Thêm sinh viên
        </button>
        <button
          className="btn btn-info"
          onClick={() => {
            this.props.handleSvUpdate(this.state.user.svId, this.state.user);
          }}
        >
          Cập nhật sinh viên
        </button>
      </div>
    );
  }
}
