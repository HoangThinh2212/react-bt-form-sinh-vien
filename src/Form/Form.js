import React, { Component } from "react";
import SinhVienForm from "./SinhVienForm";
import SinhVienTable from "./SinhVienTable";

export default class Form extends Component {
  state = {
    sinhVienList: [],
    svEdited: null,
  };
  handleSinhVienAdd = (newSv) => {
    let cloneSinhVienList = [...this.state.sinhVienList, newSv];
    this.setState({ sinhVienList: cloneSinhVienList });
  };
  handleSinhVienRemove = (userId) => {
    let index = this.state.sinhVienList.findIndex((item) => {
      return item.svId === userId;
    });
    if (index !== -1) {
      let cloneSvList = [...this.state.sinhVienList];
      cloneSvList.splice(index, 1);
      this.setState({ sinhVienList: cloneSvList });
    }
  };
  handleEditSinhVien = (value) => {
    this.setState({ svEdited: value });
  };
  handleSvUpdate = (userId, newValue) => {
    let index = this.state.sinhVienList.findIndex((item) => {
      return item.svId === userId;
    });
    if (index !== -1) {
      let cloneSvList = [...this.state.sinhVienList];
      cloneSvList[index] = newValue;
      this.setState({ sinhVienList: cloneSvList });
    }
  };
  render() {
    return (
      <div className="py-5 px-5">
        <SinhVienForm
          svEdited={this.state.svEdited}
          handleSinhVienAdd={this.handleSinhVienAdd}
          handleSvUpdate={this.handleSvUpdate}
        />
        <SinhVienTable
          handleSinhVienRemove={this.handleSinhVienRemove}
          handleEditSinhVien={this.handleEditSinhVien}
          handleSvUpdate={this.handleSvUpdate}
          sinhVienList={this.state.sinhVienList}
        />
      </div>
    );
  }
}
