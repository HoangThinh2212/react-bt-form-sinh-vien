import React, { Component } from "react";

export default class SinhVienTable extends Component {
  renderSvTable = () => {
    return this.props.sinhVienList.map((item, key) => {
      return (
        <tr>
          <td>{item.svId}</td>
          <td>{item.name}</td>
          <td>{item.phone}</td>
          <td>{item.email}</td>
          <td>
            <button
              className="btn btn-danger mr-2"
              onClick={() => {
                this.props.handleSinhVienRemove(item.svId);
              }}
            >
              Xóa
            </button>
            <button
              className="btn btn-warning"
              onClick={() => {
                this.props.handleEditSinhVien(item, item.svId);
              }}
            >
              Sửa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead className="bg-dark text-white">
          <tr>
            <th>Mã SV</th>
            <th>Họ tên</th>
            <th>Số điện thoại</th>
            <th>Email</th>
            <th>Action</th>
          </tr>
        </thead>

        <tbody>{this.renderSvTable()}</tbody>
      </table>
    );
  }
}
